<?php
/**
	Declare class that will calculate Total wine bottle sold
*/

class WineSaleCalculation{
	
	public $fileName;
	
	/* Constructor */
	
	function __construct($fileName){
		$this->fileName = $fileName;
	}
	
	/**
		Function to find the result from the person choices
		$personChoices: file selected

	*/
	
	public function getResult(){
		
		
		$personWineList			= array();
		$uniqueWineIds 		    = array();
		$wineSoldCount 			= 0;
		$finalWineData 			= array();
		
		/*Open the selected file to readout the data from it.*/
		
		$file = fopen($this->fileName,"r");
		
		/* Make an array of Wine Ids for further usage. */
		
		while (($line = fgets($file)) !== false) {
			$getSingleLine = explode("\t", $line);
			
			$pName = trim($getSingleLine[0]);
			$wId = trim($getSingleLine[1]);

			if(!array_key_exists($wId, $personWineList)){
				$personWineList[$wId] = [];
			}
			$personWineList[$wId][] = $pName;
			$uniqueWineIds[] = $wId;
		}
		fclose($file);
		
		/* Make array of unique wine Ids */
		$uniqueWineIds = array_unique($uniqueWineIds);
		
		
		foreach ($uniqueWineIds as $key => $wine) {
			$maxSize = count($wine);
			$counter = 0;

			while($counter<10){
				
				$i = intval(floatval(rand()/(float)getrandmax()) * $maxSize);
				$pName = @$personWineList[$wine][$i];
				if(!array_key_exists($pName, $finalWineData)){
					$finalWineData[$pName] = [];
				}
				if(count($finalWineData[$pName])<3){
					$finalWineData[$pName][] = $wine;
					$wineSoldCount++;
					break;
				}
				$counter++;
			}
		}


		/* List of Results */
		$resultToWrite = fopen("resultData.txt", "w");
		fwrite($resultToWrite, "Total number of wine bottles sold in aggregate : ".$wineSoldCount."\n");
		foreach (array_keys($finalWineData) as $key => $person) {
			foreach ($finalWineData[$person] as $key => $wine) {
				fwrite($resultToWrite, $person." ".$wine."\n");
			}
		}
		fclose($resultToWrite);
					
	}
}
$wscObj = new WineSaleCalculation("person_wine_3.txt");
$wscObj->getResult();
?>